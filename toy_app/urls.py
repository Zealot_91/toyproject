from django.urls import path
from . import views
app_name = 'my_app'
urlpatterns = [
    path('upload/<int:pk>', views.UserUpdateView.as_view(), name='upload'),
    path('<int:pk>', views.UserDetailView.as_view(), name='details'),
    path('imprimir_archivo', views.proc_dem_file, name="imprimir"),
    path('suavizar', views.funcion_dma, name="dma"),
    path('produc_list', views.ProductListView.as_view(), name='product_list'),
]
