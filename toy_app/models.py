from django.contrib.auth.models import AbstractUser
from django.contrib.postgres.fields import ArrayField
from django.db import models
from django.urls import reverse

from toy_app.managers import UserManager


class User(AbstractUser):
    email = models.EmailField(unique=True)
    file_one = models.FileField(upload_to='csvfiles')
    file_two = models.FileField(upload_to='csvfiles')
    objects = UserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    def get_absolute_url(self):
        return reverse("index")


class Product(models.Model):
    owner = models.ForeignKey('User', on_delete=models.CASCADE, related_name="productos")
    material = models.CharField(max_length=255, blank=False, null=False)
    oficina = models.CharField(max_length=255, blank=False, null=False)
    lote_prod = models.IntegerField(null=True)
    lote_orden = models.IntegerField(null=True)
    punto_reorden = models.IntegerField(null=True)
    texto_breve_material = models.CharField(max_length=255)
    demanda_promedio = models.IntegerField(default=0)
    tipo_de_producto = models.CharField(max_length=1, null=True)
    lead_time = models.IntegerField(null=True)
    valor_r = models.DecimalField(max_digits=4, decimal_places=3, null=True)
    valor_v = models.IntegerField(null=True)
    valor_B1 = models.IntegerField(null=True)
    valor_B2 = models.DecimalField(max_digits=4, decimal_places=3, null=True)
    valor_B3 = models.DecimalField(max_digits=4, decimal_places=3, null=True)
    valor_B4 = models.IntegerField(null=True)
    valor_k = models.DecimalField(max_digits=4, decimal_places=3, null=True)
    valor_a = models.IntegerField(null=True)
    valor_p1 = models.DecimalField(max_digits=4, decimal_places=3, null=True)
    valor_p2 = models.DecimalField(max_digits=4, decimal_places=3, null=True)
    valor_sl = models.IntegerField(null=True)
    valor_z = models.IntegerField(null=True)
    valor_R2 = models.IntegerField(null=True)
    valor_tbs = models.IntegerField(null=True)


class ProductDemand(models.Model):
    product = models.ForeignKey(Product, related_name='demands', on_delete=models.CASCADE)
    date = models.DateField()
    mes = models.IntegerField()
    anno = models.IntegerField()
    demand = models.IntegerField()

    def getKey(self):
        # pretty sure this does not work like this.
        return self.date



class PronosticoSES(models.Model):
    owner = models.ForeignKey(Product, related_name='SES', on_delete=models.CASCADE)
    monthly_demand = ArrayField(models.IntegerField(default=[0], null=True, blank=True, ))
    alpha = models.DecimalField(max_digits=3, decimal_places=3, default=0.91)


class PronosticoDMA(models.Model):
    owner = models.ForeignKey(Product, related_name='DMA', on_delete=models.CASCADE)
    demanda_dma = models.IntegerField()



class LoteEcoOrden(models.Model):
    owner = models.ForeignKey(Product, related_name='EOQ', on_delete=models.CASCADE)
    Q = models.DecimalField(max_digits=20, decimal_places=3, default=0)
    # c = owner.valor_v
    # h = owner.valor_r
    # k = owner.valor_a
    date = models.DateField(null=True)
    demand = models.IntegerField(null=True)
    costo_p = models.DecimalField(max_digits=20, decimal_places=3)
    mantener_inv = models.DecimalField(max_digits=20, decimal_places=3)


class PuntoReorden(models.Model):
    owner = models.ForeignKey(Product, related_name='reorden', on_delete=models.CASCADE)
    demand = models.IntegerField()
    date = models.DateField()
    reordenpoint = models.IntegerField()


class LoteEcoProd(models.Model):
    owner = models.ForeignKey(Product, related_name='EPQ', on_delete=models.CASCADE)
    date = models.DateField()
    Qprima = models.DecimalField(max_digits=20, decimal_places=3, default=0)

