from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from toy_app.models import User,Product,ProductDemand, PronosticoDMA

admin.site.register(User, UserAdmin)
admin.site.register(Product)
admin.site.register(ProductDemand)
admin.site.register(PronosticoDMA)
