from rest_framework.views import APIView
from rest_framework.response import Response
# from rest_framework import authentication, permissions
from django.contrib.auth.models import User
from django.http import JsonResponse

from datetime import datetime
from math import sqrt

from django.shortcuts import render, redirect
from django.views import View
from django.views.generic import UpdateView, DetailView, ListView
from django.contrib.auth.decorators import login_required

from toy_app import forms
from toy_app.models import User, ProductDemand, Product, \
    PronosticoSES, LoteEcoOrden, LoteEcoProd, PronosticoDMA

import json
import csv


def index(request):
    return render(request, 'toy_app/index.html')


class ProductListView(ListView):
    model = Product


class Register(View):
    form_class = forms.SignUpForm
    template = 'registration/signup.html'

    def get(self, request):
        # mostrar formulario de registro
        form = self.form_class()
        return render(request, self.template, {'form': form})

    def post(self, request):
        # procesar formulario de registro
        form = self.form_class(request.POST)

        if form.is_valid():
            user = form.save(commit=False)
            user.username = user.email
            user.save()

            return render(request, 'toy_app/index.html')
        else:
            print('error')
            print(form.errors)
            return render(request, self.template, {'form': form})


class UserUpdateView(UpdateView):
    fields = ("file_one", "file_two")
    model = User


class UserDetailView(DetailView):
    context_object_name = "product_detail"
    model = Product
    template_name = 'toy_app/User_detail_view.html'


@login_required
def proc_dem_file(request):
    demfile = request.user.file_one.path
    varfile = request.user.file_two.path
    with open(demfile, newline='', encoding='latin-1') as f:

        reader = csv.reader(f, delimiter=';', skipinitialspace=True, strict=True)
        firstline = True
        for row in reader:
            if firstline:
                firstline = False
            else:
                material = row[1]
                oficina = row[0]
                txt_breve = row[2]
                product, created = Product.objects.get_or_create(material=material, owner=request.user,
                                                                 oficina=oficina,
                                                                 texto_breve_material=txt_breve)
                date_string = row[3]
                date = datetime.strptime(date_string, '%d/%m/%Y')
                mes_string = row[4]
                anno_string = row[5]
                demand_string = row[6]
                demand = int(demand_string)
                mes = int(mes_string)
                anno = int(anno_string)
                ProductDemand.objects.get_or_create(
                    product=product,
                    date=date,
                    mes=mes,
                    anno=anno,
                    demand=demand)

    with open(varfile, newline='') as f:

        reader = csv.reader(f, delimiter=';', skipinitialspace=True, strict=True)
        firstline = True
        for row in reader:
            if firstline:
                print(row)
                firstline = False
            else:
                if row[0] == '':
                    break

                tipo_de_producto = row[0]
                material = row[1]
                lead_time = int(float(row[2]))
                valor_r = float(row[3])
                valor_v = int(float((row[4])))
                valor_B1 = int(float((row[5])))
                valor_B2 = float(row[6])
                valor_B3 = float(row[7])
                valor_B4 = int(float((row[8])))
                valor_k = float(row[9])
                valor_a = int(float((row[10])))
                valor_p1 = float(row[11])
                valor_p2 = float(row[12])
                valor_sl = int(float((row[13])))
                valor_z = int(float((row[14])))
                valor_R2 = int(float((row[15])))
                valor_tbs = int(float((row[16])))
                prd = Product.objects.get(material=material, owner=request.user)
                prd.tipo_de_producto = tipo_de_producto
                prd.lead_time = lead_time
                prd.valor_r = valor_r
                prd.valor_z = valor_z
                prd.valor_tbs = valor_tbs
                prd.valor_R2 = valor_R2
                prd.valor_sl = valor_sl
                prd.valor_p2 = valor_p2
                prd.valor_p1 = valor_p1
                prd.valor_B4 = valor_B4
                prd.valor_B3 = valor_B3
                prd.valor_B2 = valor_B2
                prd.valor_B1 = valor_B1
                prd.valor_v = valor_v
                prd.valor_a = valor_a
                prd.valor_k = valor_k
                prd.save()
    print("termino la lectura")
    funcion_dma(request)
    calcular_EPQ(request)
    return render(request, 'toy_app/index.html')


@login_required
def funcion_dma(request):
    print("inicio demanda")
    p = Product.objects.filter(owner=request.user)
    f = open('DMA.txt', 'w')
    periodos = 60
    for product in p:
        all_demands = []
        dma = []
        sma = []
        n = 0
        y = []
        demanda_muestra = 0
        cont = 0
        for item in product.demands.all().order_by('date'):

            if cont < (len(product.demands.all()) - periodos):
                all_demands.append(item)
                demanda_muestra += item.demand
            else:
                product.demanda_promedio = demanda_muestra / cont
                break
            cont += 1

        while n + periodos - 1 < len(all_demands):
            aux = 0
            for i in range(n, n + periodos):
                aux = aux + all_demands[i].demand

                # (all_demands[n].demand + all_demands[n + 1].demand) / 2
            aux = aux / periodos
            sma.append(aux)
            n += 1
        n = 0
        while n - 1 + periodos < len(sma):
            aux = 0

            for i in range(n, n + periodos):
                aux = aux + sma[i]
            aux = aux / periodos
            dma.append(aux)
            n += 1
        a = ((2 * sma[-1]) - dma[-1])
        b = (2 * (sma[-1] - dma[-1]) / periodos)

        f.write(product.material + "\n")
        for num in range(1, periodos + 1):
            var = round(a + (num * b))
            y.append(var)
            PronosticoDMA.objects.get_or_create(owner=product, demanda_dma=var)

            f.write(str(var) + "\n")

    f.close()
    print("termino de calcular media movil doble")
    return render(request, 'toy_app/product_list.html')


@login_required
def metodo_ses(request):
    p = Product.objects.filter(owner=request.user)
    flag = True
    forecast = 0
    n = 1

    for i in p:
        lista = PronosticoSES.objects.filter(owner=i)
        for f in lista.monthly_demand:

            if len(f) == 12:
                if flag:
                    forecast = f[0]
                    flag = False
                else:
                    forecast = forecast * 0.9 * (f[n - 1] - forecast)
                    n += 1
        i.demanda_ses = forecast


class ProductView(ListView):
    # context_object_name = "productos"
    model = Product


def CalcularEOQ(request):
    p = Product.objects.filter(owner=request.user)
    f = open('EOQ.txt', 'w')
    for item in p:
        lote = LoteEcoOrden.objects.create(owner=item)
        lote.demand = item.demanda_promedio
        c = item.owner.valor_v
        h = item.owner.valor_r
        k = item.owner.valor_a
        root = (k * 2 * item.demanda_promedio) / h
        root = sqrt(root)
        lote.Q = root
        item.lote_orden = root
        lote.costo_p = k + c * root
        lote.mantener_inv = (h * root ** 2) / (2 * lote.demand)

        lote.save()
        item.save()
        f.write(root)


def calcular_EPQ(request):
    p = Product.objects.filter(owner=request.user)
    for item in p:
        lote = LoteEcoProd.objects.create(owner=item)
        lote.demand = item.demanda_promedio
        d = item.demanda_promedio
        c = item.owner.valor_v
        h = item.owner.valor_r
        k = item.owner.valor_a
        root = (k * 2 * item.demanda_promedio) / (h * (1 - (d / p)))
        root = sqrt(root)
        lote.Qprima = root
        item.lote_prod = root
        lote.save()
        item.save()


def calc_stats(request):
    p = Product.objects.filter(owner=request.user)
    for product in p:
        DMA = PronosticoDMA.objects.filter(owner=p)
        suma = 0
        for item in DMA:
            suma += item.demanda_dma

        D = product / len(DMA)
        H = product.valor_r
        K = product.valor_a
        Q = sqrt((K * 2 * D) / H)
        Q_prima = sqrt((K * 2 * D) / (H * (1 - (D / 1))))
        product.lote_orden = Q
        product.lote_prod = Q_prima
        product.save()


def get_data(request, *args, **kwargs):
    data = {
        "sales":200,
        "customer":20,
    }

    return JsonResponse(data, safe=False)


class ChartData(APIView):
    authentication_classes = []  # (authentication.TokenAuthentication,)
    permission_classes = []  # (permissions.IsAdminUser,)

    def get(self, request, format=None):

        data = {
            "sales": 200,
            "customers": 20,
            "users": User.objects.all().count()
        }
        return Response(data)
